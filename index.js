function getIndentation(content) {
    const ws = [ ' ', '\t' ];

    function getIndent(line) {
        for (const i in ws) {
            const type = ws[i];

            if (line.trimEnd().length && line.startsWith(type)) {
                let count = 0;
                while (line[count] === type) {
                    count += 1;
                }

                let res = '';
                for (let i=0; i<count; i++) { res += type; }
                return res;
            }
        }

        return '';
    }

    const levels = [];
    const lines = content.split('\n');

    let error = false;
    let lastLevel = 0;
    let firstIndent = '';

    for (const lnum in lines) {
        const line = lines[lnum];
        const indentation = getIndent(line);

        if (!indentation) {
            levels.push(0);
            continue;
        }

        if (!firstIndent) {
            firstIndent = indentation;
        }

        if (firstIndent[0] != indentation[0]) {
            error = true;
            console.log(`ERR - indentation of spaces and tabs mixed (L${lnum})`);
        }
        if (indentation.length % firstIndent.length !== 0) {
            error = true;
            console.log('ERR - indentation should be multiple of '
                + `${firstIndent.length} (L${lnum})`);
        }

        const indentLevel = indentation.length / firstIndent.length;
        if (indentLevel-lastLevel > 1) {
            error = true;
            console.log(`ERR - indentation jumps of multiple levels (L${lnum})`);
        }

        if (!error) {
            levels.push(indentLevel);
            lastLevel = indentLevel;
        }
    }

    if (error) {
        throw "ERR - indentation of source is incorrect";
    }

    return levels;
}

function parse(content, nested=false) {
    const indentation = getIndentation(content);

    const res = {};
    const levelsName = [];
    const levelsRest = [];

    const lines = content.split('\n');
    for (const lnum in lines) {
        const line = lines[lnum].trim();
        const indent = indentation[lnum];

        if (line.length == 0) { continue; }

        const name = line.split(' ')[0];
        const rest = line.split(' ').slice(1).join(' ');

        if (indent < levelsName.length+1) {
            levelsName.push('');
            levelsRest.push('');
        }

        let past = indent ? levelsName[indent-1] : '';
        levelsName[indent] = past ? past + ' ' + name : name;

        past = indent ? levelsRest[indent-1] : '';
        levelsRest[indent] = past ? past + ' ' + rest : rest;

        if (!nested) {
            res[levelsName[indent]] = levelsRest[indent];
        } else {
            let temp = res;
            const vals = levelsName[indent].split(' ');

            for (const i in vals) {
                if (!temp[vals[i]]) {
                    temp[vals[i]] = {};
                }
                temp = temp[vals[i]];
            }
            temp._ = levelsRest[indent];
        }
    }

    return res;
}

const fs = require('fs');
const fname = process.argv[2];

if (fname) {
    console.log(JSON.stringify(parse(fs.readFileSync(fname).toString(), true), null, 2));
} else {
    console.log('usage: node index.js [filename.txt]');
}
